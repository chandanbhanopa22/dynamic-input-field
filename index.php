<?php
    require_once("class.database.php");
    if(isset($_POST) && !empty($_POST)){
        
        $formName = $_POST['form_name'];
        //json_encode($_POST['fields']);
        $fields = $_POST['fields'];
        print_r($fields);
        $html = "<div class='container'>";
        $html .= "<div class='panel panel-default'>";
        $html .= "<div class='panel-heading'>".$formName."</div>";
        $html .= "<div class='panel-body'>";
        $html .= "<form  name='custom-form' action='index.php' method='POST' class='input-form'>";
        $counter = 0;
        $dataArray = array();
        $finalArray = array();
        foreach($fields as $key => $fieldArray) {
            $count = count($fieldArray);
            $innerCounter = 0;
            foreach($fieldArray as $value) {
                $dataArray[$innerCounter][$key] = $value;
                $innerCounter++;
            }
        }
        
        print_r($dataArray);
        die();

        foreach($dataArray as $key=>$input) {
            $htmlInput= "";
            $htmlRadio = "";
            $htmlCheckbox = "";
            $htmlSelect = "";
            if($input['input-type'] == "text") {
                $html = $html.getInputHtml($input['title'], "text","", $input['required']? $input['required'] : 0);
            }
            if($input['input-type'] == "checkbox") {    

                $html = $html.getInputHtml($input['title'], "checkbox", $input['type-options'], isset($input['required'])? 1 : 0);
            }
            if($input['input-type'] == "radio") {
                $html = $html.getInputHtml($input['title'], "radio", $input['type-options'],  isset($input['required'])? 1 : 0);      
            }
            if($input['input-type'] == "select") {
                $html = $html.getInputHtml($input['title'], "select", $input['type-options'],  isset($input['required'])? 1 : 0);

            }
        }

        $html .= "</form>";
        $html .= "</div>";
        $html .= "</div>";
        $html .= "</div>";
        $html .= "</div>";
        echo $html;
    }

    function getInputHtml($title, $type, $options, $required = 0){

            $htmlInput = "";
            $htmlInput .= "<div class='form-group row'>";
            $htmlInput .= "<div class='col-xs-4'>";
            $htmlInput .= "<label>".$title."</label>";
            if($required) {
                $htmlInput .= "<span style='color:red;font-size:25px;'>*</span>";    
            }
            if($type == "text") {
                $htmlInput .= "<input class='form-control' name='".strtolower(str_replace(" ","_",$title))."' type='text' />";
            }
            if($type == "checkbox") {
                $selectArray =  explode(",", $options);
                $htmlInput .= "<div class='form-check-inline checkbox'>";
                foreach($selectArray as $key=>$value) {
                    $htmlInput .= "<label class='form-check-label'>";
                    $htmlInput .= "<input type='checkbox' class='form-check-input' value=''>".$value;
                    $htmlInput .= "</label>";   
                }
               
                $htmlInput .= "</div>";

            }
            if($type == "radio") {
                $htmlInput .= "<div class='form-check-inline radio'>";
                $selectArray =  explode(",", $options);
                foreach($selectArray as $key=>$value) {
                    $htmlInput .= "<label class='form-check-label'>";
                    $htmlInput .= "<input type='radio' class='form-check-input' name='optradio' />".$value;
                    $htmlInput .= "</label>";    
                }
                 
                 $htmlInput .= "</div>";
            }
            if($type == "select") {
                $htmlInput .=   "<select class='form-control type' id = 'type_1' name='fields[input-type][]'>";
                $htmlInput .=   "<option value=''>Select</option>";
                $selectArray =  explode(",", $options);
                foreach($selectArray as $key=>$value) {
                    $htmlInput .=   "<option value='".$value."'>".$value."</option>";    
                }
                $htmlInput .=   "</select>";
            }

            
            $htmlInput .= "</div>";
            $htmlInput .= "</div>";
            return $htmlInput;

        } 
?>

<html lang="en">
    <head>
        <title>Create Dynamic Form</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <style type="text/css">
            .addmorebtn{
            margin-top: 24px;
            }
            button.btn.btn-danger.remove {
            margin-top: 25px;
            }
            .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
                position: absolute;
                margin-top: 4px\9;
                margin-left: -15px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-heading">Dynamic Form</div>
                <div class="panel-body">
                    <form  name="custom-form" action="index.php" method="POST" class="input-form">
                        <div class="form-group row">
                            <div class="col-xs-2">
                                <label for="ex1">Form Name</label>
                                <input class="form-control" name="form_name" id="ex1" type="text">
                            </div>
                            <div class="col-xs-3">
                                <label for="ex2">Form Action </label>
                                <input class="form-control" name="form_action" id="ex2" type="text" />
                            </div>
                        </div>
                        <div class="form-group row" id="after-add-more">
                            <div class="col-xs-2">
                                <label for="ex1">Title</label>
                                <input class="form-control title_1" name="fields[title][]"  type="text" />
                            </div>
                            <div class="col-xs-2">
                                <label for="sel1">Select list:</label>
                                <select class="form-control type" id = "type_1" name="fields[input-type][]">
                                    <option value="text">text</option>
                                    <option value="radio">radio</option>
                                    <option value="select">select</option>
                                    <option value="checkbox">checkbox</option>
                                    <option value="textarea">textarea</option>
                                </select>
                            </div>
                            <div class="col-xs-3 hide" id = "col_1">
                                <label for="ex1">Options (comma seperated)</label>
                                <input class="form-control " type="text"  name="fields[type-options][]" />
                            </div>
                            <div class="col-xs-2">
                                <label>Required</label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name ="fields[required][]" value="1" />Required</label>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <button type="button" class="btn btn-success addmorebtn add-more"><i class="glyphicon glyphicon-plus"></i>Add More</button>
                            </div>
                        </div>
                        <div class="control-group text-center submit-div">
                            <br>
                            <button class="btn btn-success" type="submit" name="submit" value="submit">Success</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                var counter = 1;

                $("body").on("click",".add-more", function(){
                    let parentId = $(this).parent().parent().attr("id");
                    console.log($("#after-add-more").siblings());
                    $(".submit-div").before(addRows(counter));
                    counter++;
                    
                });

               
                function addRows(counter) {
                    let htmlString = '\
                        <div class="form-group row vertual-row" id= "row_id_'+counter+'">\
                            <div class="col-xs-2">\
                                <label for="ex1">Title</label>\
                                <input class="form-control title_'+counter+'" type="text"  name="fields[title][]" />\
                            </div>\
                            <div class="col-xs-2">\
                                <label for="sel1">Select list:</label>\
                                <select class="form-control type" id = "type_'+counter+'" name="fields[input-type][]">\
                                    <option value="text">text</option>\
                                    <option value="radio">radio</option>\
                                    <option value="select">select</option>\
                                    <option value="checkbox">checkbox</option>\
                                </select>\
                            </div>\
                            <div class="col-xs-3 hide" id = "col_'+counter+'">\
                                <label for="ex2">Enter Options (, seperated)</label>\
                                <input class="form-control type-options_'+counter+'" type="text" name="fields[type-options][]" />\
                            </div>\
                            <div class="col-xs-2">\
                                <label for="ex3">Required</label>\
                                <div class="checkbox">\
                                    <label><input type="checkbox"  class="require_'+counter+'" value="1" name ="fields[required][]" />Required</label>\
                                </div>\
                            </div>\
                            <div class="col-xs-2">\
                                <button class="btn btn-danger remove" type="button remove'+counter+'"><i class="glyphicon glyphicon-remove"></i> Remove</button>\
                            </div>\
                        </div>\
                    ';
                    return htmlString;

                }
                $("body").on("click",".remove",function(){ 
                    $(this).parent().parent().remove();
                });

                $("body").on("change",".type", function(){
                    let typeId = $(this).attr("id");
                    let rowId = typeId.split("_").pop();
                    let typeValue = $(this).val();
                    if(typeValue != "text") {
                        $("div#col_"+rowId).removeClass("hide");
                    } else {
                         $("div#col_"+rowId).addClass("hide");
                    }
                    
                    console.log(rowId);
                    console.log($(this).val());
                });
            
            
            });
                
            
        </script>
    </body>
</html>