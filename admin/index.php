<html lang="en">
    <head>
        <title>Create Dynamic Form</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <style type="text/css">
            .addmorebtn{
            margin-top: 24px;
            }
            button.btn.btn-danger.remove {
            margin-top: 25px;
            }
            .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
                position: absolute;
                margin-top: 4px\9;
                margin-left: -15px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-heading">Create your forms</div>
                <div class="panel-body">
                    <form  name="custom-form" action="step_one.php" method="POST" class="input-form">
                        <div class="form-group row">
                            <div class="col-xs-12">
                                <label for="ex1">How many form would you like to create?</label>
                                <input class="form-control" name="no_of_forms" type="text">
                            </div>
                        </div>
                        <div class="control-group text-center submit-div">
                            <br>
                            <button class="btn btn-success" type="submit" name="submit" value="next">Next</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>