<?php
    $html = "";
    $formName = "";
    require_once("../class.database.php");
    function createInnerArray($fields){
            $dataArray = array();
            foreach($fields as $key => $fieldArray) {
                $innerCounter = 0;
                foreach($fieldArray as $value) {
                    $dataArray[$innerCounter][$key] = $value;
                    $innerCounter++;
                }
            }
            return $dataArray;
    }

    if(isset($_POST) && !empty($_POST)){
        $formArray = array();
        $post = $_POST;
        /*
        $array[] = array(
            "form_name"=>"registraiton",
            "form_action"=>"registration.php",
            "form_field"=>array(
                array("title"=>"username","type"=>"text","required"=>1,"options"=>"1,2,3"),
                array("title"=>"username","type"=>"text","required"=>1,"options"=>"1,2,3"),
                array("title"=>"username","type"=>"text","required"=>1,"options"=>"1,2,3")
            )
        );
        $array[] = array(
            "form_name"=>"registraiton",
            "form_action"=>"registration.php",
            "form_field"=>array(
                array("title"=>"username","type"=>"text","required"=>1,"options"=>"1,2,3"),
                array("title"=>"username","type"=>"text","required"=>1,"options"=>"1,2,3"),
                array("title"=>"username","type"=>"text","required"=>1,"options"=>"1,2,3")
            )
        );
        */
        //print_r( $array);
        $counter = 0;
        foreach($post as $key => $innerArray) {
            $counterOne = 0;
            if(is_array($innerArray)) {
                foreach($innerArray as $innerKey=> $innerValue){
                    $formArray[$counterOne][$key] = $innerValue;
                        if($key == "form_field"){
                        $formArray[$counterOne][$key] = createInnerArray($innerValue);  
                    }
                    $counterOne++;  
                }    
            }
           
            
        }

        //json_encode($_POST['fields']);
        //$fields = $_POST['fields'];
        foreach ($formArray as $key => $fields) {
            $html .= createFormField($fields);   
        }
        
        
    }

    function createFormField($fields){
        $html = "";
        $html .= "<div class='container'>";
        $html .= "<div class='panel panel-default'>";
        $html .= "<div class='panel-heading'>".$fields['form_name']."</div>";
        $html .= "<div class='panel-body'>";
        $html .= "<form  name='custom-form' action='index.php' method='POST' class='input-form'>";
        $counter = 0;
        $dataArray = $fields['form_field'];

        foreach($dataArray as $key=>$input) {
            $htmlInput= "";
            $htmlRadio = "";
            $htmlCheckbox = "";
            $htmlSelect = "";
            if($input['input-type'] == "text") {
                $html = $html.getInputHtml($input['title'], "text","", isset($input['required'])? 1 : 0);
            }
            if($input['input-type'] == "checkbox") {    
                $html = $html.getInputHtml($input['title'], "checkbox", $input['type-options'], isset($input['required'])? 1 : 0);
            }
            if($input['input-type'] == "radio") {
                $html = $html.getInputHtml($input['title'], "radio", $input['type-options'],  isset($input['required'])? 1 : 0);      
            }
            if($input['input-type'] == "select") {
                $html = $html.getInputHtml($input['title'], "select", $input['type-options'],  isset($input['required'])? 1 : 0);
            }
        }

        $html .= "</form>";
        $html .= "</div>";
        $html .= "</div>";
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    function getInputHtml($title, $type, $options, $required = 0){

            $htmlInput = "";
            $htmlInput .= "<div class='form-group row'>";
            $htmlInput .= "<div class='col-xs-4'>";
            $htmlInput .= "<label>".$title."</label>";
            if($required) {
                $htmlInput .= "<span style='color:red;font-size:25px;'>*</span>";    
            }
            if($type == "text") {
                $htmlInput .= "<input class='form-control' name='".strtolower(str_replace(" ","_",$title))."' type='text' />";
            }
            if($type == "checkbox") {
                $selectArray =  explode(",", $options);
                $htmlInput .= "<div class='form-check-inline checkbox'>";
                foreach($selectArray as $key=>$value) {
                    $htmlInput .= "<label class='form-check-label'>";
                    $htmlInput .= "<input type='checkbox' class='form-check-input' value=''>".$value;
                    $htmlInput .= "</label>";   
                }
               
                $htmlInput .= "</div>";

            }
            if($type == "radio") {
                $htmlInput .= "<div class='form-check-inline radio'>";
                $selectArray =  explode(",", $options);
                foreach($selectArray as $key=>$value) {
                    $htmlInput .= "<label class='form-check-label'>";
                    $htmlInput .= "<input type='radio' class='form-check-input' name='optradio' />".$value;
                    $htmlInput .= "</label>";    
                }
                 
                 $htmlInput .= "</div>";
            }
            if($type == "select") {
                $htmlInput .=   "<select class='form-control type' id = 'type_1' name='fields[input-type][]'>";
                $htmlInput .=   "<option value=''>Select</option>";
                $selectArray =  explode(",", $options);
                foreach($selectArray as $key=>$value) {
                    $htmlInput .=   "<option value='".$value."'>".$value."</option>";    
                }
                $htmlInput .=   "</select>";
            }

            if($type == "textarea") {
                $htmlInput .=   "<div class='form-group'>";
                $htmlInput .=   "<label for='comment'>".$title."</label>";
                $htmlInput .=   "<textarea class='form-control' rows='5' id='comment'></textarea>";
                $htmlInput .=   "</div>";
            }            

            
            $htmlInput .= "</div>";
            $htmlInput .= "</div>";
            return $htmlInput;

        } 
?>

<html lang="en">
    <head>
        <title><?php echo $formName; ?></title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <style type="text/css">
            .addmorebtn{
            margin-top: 24px;
            }
            button.btn.btn-danger.remove {
            margin-top: 25px;
            }
            .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
                position: absolute;
                margin-top: 4px\9;
                margin-left: -15px;
            }
        </style>
    </head>
    <body>
        <form>
            <?php echo $html; ?>
            <div class="form-group row">
                <div class="control-group text-center ">
                    <button class="btn btn-success" id="button_no_<?php echo $i; ?>" type="submit" name="submit" value="submit">Submit</button>
                    </div>
            </div>
        </form>
    </body>
</html>