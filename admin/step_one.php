<?php
$formCounter = 0;
if(isset($_POST) && $_POST['submit'] == 'next'){
    $formCounter = $_POST['no_of_forms'];
}
?>
<html lang="en">
    <head>
        <title>Create Dynamic Form</title>
        <script src="../jquery.js"></script>
        <link rel="stylesheet" href="../bootstrap.css">
        <style type="text/css">
            .addmorebtn{
            margin-top: 24px;
            }
            button.btn.btn-danger.remove {
            margin-top: 25px;
            }
            .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
                position: absolute;
                margin-top: 4px\9;
                margin-left: -15px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <form  name="custom-form" action="save.php" method="POST" class="input-form">
            <?php for($i = 0 ; $i < $formCounter ; $i ++ ) {
                    $formField = "form_field[".$i."]";
             ?>
                <div class="panel panel-default form_<?php echo $i; ?>">
                    <div class="panel-heading">Form <?php echo $i ;?></div>
                    <div class="panel-body" id="formpanel_<?php echo $i; ?>">
                            <div class="form-group row">
                                <div class="col-xs-2">
                                    <label for="ex1">Form Name</label>
                                    <input class="form-control" name="form_name[<?php echo $i; ?>]"  type="text">
                                </div>
                                <div class="col-xs-3">
                                    <label for="ex2">Form Action </label>
                                    <input class="form-control" name="form_action[<?php echo $i; ?>]"  type="text" />
                                </div>
                            </div>
                            <div class="form-group row" id="after-add-more">
                                <div class="col-xs-2">
                                    <label for="ex1">Title</label>
                                    <input class="form-control title_1" name="<?php echo $formField;?>[title][]"  type="text" />
                                </div>
                                <div class="col-xs-2">
                                    <label for="sel1">Select list:</label>
                                    <select class="form-control type" id = "type_1" name="<?php echo $formField;?>[input-type][]">
                                        <option value="text">text</option>
                                        <option value="radio">radio</option>
                                        <option value="select">select</option>
                                        <option value="checkbox">checkbox</option>
                                        
                                    </select>
                                </div>
                                <div class="col-xs-3 hide" id = "col_1">
                                    <label for="ex1">Options (comma seperated)</label>
                                    <input class="form-control " type="text"  name="<?php echo $formField;?>[type-options][]" />
                                </div>
                                <div class="col-xs-2">
                                    <label>Required</label>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name ="<?php echo $formField;?>[required][]" value="1" />Required</label>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <button type="button" class="btn btn-success addmorebtn add-more_<?php echo $i;?>" id="parentaddmore_<?php echo $i; ?>"><i class="glyphicon glyphicon-plus"></i>Add More</button>
                                </div>
                            </div>
                            
                       <div class="control-group text-center submit-div_<?php echo $i; ?>">
                        
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
            $(document).ready(function() {
                var counter = 1;
                var fields = "<?php echo $formField;?>";
                var addMore = ".add-more_<?php echo $i;?>";
                $("body").on("click",addMore, function(){
                    let buttonIdString = $(this).attr("id");
                    let idArray = buttonIdString.split("_");
                    let id = idArray.pop();
                    let parentId = $(this).parent().parent().attr("id");
                    console.log(parentId);
                    $(".submit-div_"+id).before(addRows(counter));
                    counter++;
                    
                });

               
                function addRows(counter) {
                    let htmlString = '\
                        <div class="form-group row vertual-row" id= "row_id_'+counter+'">\
                            <div class="col-xs-2">\
                                <label for="ex1">Title</label>\
                                <input class="form-control title_'+counter+'" type="text"  name="'+fields+'[title][]" />\
                            </div>\
                            <div class="col-xs-2">\
                                <label for="sel1">Select list:</label>\
                                <select class="form-control type" id = "type_'+counter+'" name="'+fields+'[input-type][]">\
                                    <option value="text">text</option>\
                                    <option value="radio">radio</option>\
                                    <option value="select">select</option>\
                                    <option value="checkbox">checkbox</option>\
                                </select>\
                            </div>\
                            <div class="col-xs-3 hide" id = "col_'+counter+'">\
                                <label for="ex2">Enter Options (, seperated)</label>\
                                <input class="form-control type-options_'+counter+'" type="text" name="'+fields+'[type-options][]" />\
                            </div>\
                            <div class="col-xs-2">\
                                <label for="ex3">Required</label>\
                                <div class="checkbox">\
                                    <label><input type="checkbox"  class="require_'+counter+'" value="1" name ="'+fields+'[required][]" />Required</label>\
                                </div>\
                            </div>\
                            <div class="col-xs-2">\
                                <button class="btn btn-danger remove" type="button remove'+counter+'"><i class="glyphicon glyphicon-remove"></i> Remove</button>\
                            </div>\
                        </div>\
                    ';
                    return htmlString;

                }
                $("body").on("click",".remove",function(){ 
                    $(this).parent().parent().remove();
                });

                $("body").on("change",".type", function(){
                    let typeId = $(this).attr("id");
                    let rowId = typeId.split("_").pop();
                    let typeValue = $(this).val();

                    let parentId = $(this).parent().parent().parent().attr("id");
                    /*form_panel#0*/
                    if(typeValue == "text"){

                        $("#"+parentId+" div#col_"+rowId).addClass("hide");
                    } else {
                        $("#"+parentId+" div#col_"+rowId).removeClass("hide");
                    }
                    /*
                    let checkOptions = ["text", "textarea"];
                    if(checkOptions.includes(typeValue)) {
                        
                    }  else {
                        $("div#col_"+rowId).removeClass("hide");
                    }*/
                    
                });
            
            
            });
                
            
        </script>
            <?php } ?>
                        <div class="control-group text-center ">
                        <br>
                        <button class="btn btn-success" id="button_no_<?php echo $i; ?>" type="submit" name="submit" value="submit">Create Form</button>
                        </div>

        </form>
        </div>
        
    </body>
</html>